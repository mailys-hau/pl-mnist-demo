import torch



class GaussianNoise():
    def __init__(self, std=0, mean=1):
        self.std = std
        self.mean = mean

    def __call__(self, t):
        return t + torch.normal(self.std, self.mean, t.size())
