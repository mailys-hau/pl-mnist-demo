from collections.abc import Mapping



def rec_update(d, u):
    """ Recursively update dict of any depth """
    for k, v in u.items():
        if isinstance(v, Mapping):
            d[k] = rec_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d
