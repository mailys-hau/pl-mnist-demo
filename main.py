import click as cli
import yaml

from copy import deepcopy
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import MLFlowLogger
from pytorch_lightning.utilities.seed import seed_everything

from data import load_data
from networks import build_model
from utils import rec_update




seed_everything(404)



@cli.group(context_settings={"help_option_names": ["-h", "--help"]})
@cli.option("-c", "--config-file", type=cli.Path(exists=True), default=None,
            help="YML to configure called command.")
@cli.option("--debug", is_flag=True,
            help="Put results in Debug DB of MLFlow.")
@cli.pass_context
def main(ctx, config_file, debug):
    with open(f"config-files/default-{ctx.invoked_subcommand}.yml", 'r') as fd:
        full_config = yaml.full_load(fd)
    if config_file:
        with open(config_file, 'r') as ffd:
            config = yaml.full_load(ffd)
        full_config = rec_update(full_config, config)
    exp_name = "Debug" if debug else f"Demo{ctx.invoked_subcommand.capitalize()}"
    ctx.obj = {"config": full_config, "exp_name": exp_name}


@main.command(name="train", short_help="Training.")
@cli.pass_context
def train(ctx):
    config = deepcopy(ctx.obj["config"])
    print("Loading data...")
    trainloader, valloader = load_data(config["data"]["dataset"].pop("name"), **config["data"])
    print("Building network...")
    net = build_model(config["network"].pop("name"), **config["network"])
    mlflog = MLFlowLogger(experiment_name=ctx.obj["exp_name"])
    trainer = Trainer(weights_summary="full", logger=mlflog,
                      callbacks=[ModelCheckpoint(monitor="v_loss")], **config["trainer"])
    print("Training...")
    trainer.fit(net, trainloader, valloader)
    # Save full training config
    trainer.logger.log_hyperparams(ctx.obj["config"])

@main.command(name="test", short_help="Testing.")
@cli.pass_context
def test(ctx):
    """ Let's see if your network work """
    config = deepcopy(ctx.obj["config"])
    print("Loading data...")
    testloader = load_data(config["data"]["dataset"].pop("name"), **config["data"], test=True)
    print("Building network...")
    if not "weights" in config["network"]:
        raise UserWarning("No weights were given for the network. Results will be random.")
    net = build_model(config["network"].pop("name"), **config["network"])
    mlflog = MLFlowLogger(experiment_name=ctx.obj["exp_name"])
    tester = Trainer(weights_summary="full", logger=mlflog, **config["tester"])
    tester.test(net, testloader)
    # Save full training config
    tester.logger.log_hyperparams(ctx.obj["config"])



if __name__ == "__main__":
    main()
