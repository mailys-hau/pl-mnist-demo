import numpy as np

from torch import from_numpy as fnp
from torch.utils.data import Dataset
from torchvision.datasets import MNIST

from utils import _noisers



class ReconstructMNIST(MNIST):
    def __init__(self, noise={"name": "GaussianNoise", "params": {"std": 0, "mean": 1}},
                 root="data-mnist", train=True, transform=None, target_transform=None, download=False):
        self.noise = _noisers[noise["name"]](**noise["params"])
        super(ReconstructMNIST, self).__init__(root, train, transform, target_transform, download)

    def __getitem__(self, i):
        img, _ = super().__getitem__(i)
        return self.noise(img), img
