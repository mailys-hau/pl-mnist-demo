import torch
import torchvision.transforms as tr

from torch.utils.data import DataLoader, random_split
from torchvision.datasets import MNIST

from data.datasets import ReconstructMNIST


_datasets = {"MNIST": MNIST, "ReconstructMNIST": ReconstructMNIST}


def load_data(name, test=False, **kwargs):
    # PyTorch Dataset sends PIL Images
    transforms = tr.Compose([tr.ToTensor(), tr.Lambda(lambda x: x / 255)])
    #tg_transform = tr.Lambda(lambda x: torch.LongTensor([x]))
    kwdataset = kwargs.pop("dataset")
    dataset = _datasets[name]
    if test:
        testset = dataset(root="mnist-data", train=False, transform=transforms, **kwdataset)
        return DataLoader(testset, **kwargs)
    else:
        data = dataset(root="mnist-data", transform=transforms, **kwdataset)
        # 10% for validation
        trainset, valset = random_split(data, [54000, 6000])
        trainloader = DataLoader(trainset, batch_size=kwargs.pop("batch_size", 16),
                                 shuffle=True, **kwargs)
        valloader = DataLoader(valset, **kwargs)
        return trainloader, valloader
