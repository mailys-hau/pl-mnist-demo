import inspect as ispc
import torch.nn as nn


from networks.autoencoders import ConvAE, DenseAE
from networks.dense import Dense
from networks.variational_autoencoders import ConvVAE
from networks.losses import KLDLoss




_all_models = {"Dense": Dense, "DenseAE": DenseAE, "ConvAE": ConvAE, "ConvVAE": ConvVAE}
_custom_losses = {"KLDLoss": KLDLoss} # Different from torch.nn.KLDivLoss



def build_model(name, loss, optimizer, weights=None, **params):
    losses = dict(ispc.getmembers(nn, ispc.isclass))
    losses.update(_custom_losses)
    loss = losses[loss.pop("name")](**loss)
    if name not in _all_models:
        raise ValueError(f"Model {name} not found. Chose a model from {list(_all_models.keys())}.")
    if weights:
        print("Loading network weights...")
        net = _all_models[name].load_from_checkpoint(weights, loss=loss,
                                                     optimizer=optimizer, **params)
    else:
        net = _all_models[name](loss=loss, optimizer=optimizer, **params)
    return net
