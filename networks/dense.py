import torch.nn as nn

from networks.core import EnhancedLightningModule



class Dense(EnhancedLightningModule):
    def __init__(self, cin=1, cout=10, dropout=0.1, loss=nn.CrossEntropyLoss(),
                 optimizer={"name": "Adam", "params": {}}, metrics=[]):
        super(Dense, self).__init__(loss, optimizer, metrics)
        self.model = nn.Sequential(nn.Flatten(),
                                   nn.Linear(28 * 28, 256),
                                   nn.ReLU(),
                                   nn.Dropout(dropout),
                                   nn.Linear(256, 64),
                                   nn.ReLU(),
                                   nn.Dropout(dropout),
                                   nn.Linear(64, cout),
                                   nn.Softmax(1))

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        err = super().training_step(batch, batch_idx)
        self.log("loss", err, prog_bar=True, on_step=True, on_epoch=True)
        return err

    def validation_step(self, batch, batch_idx):
        err = super().validation_step(batch, batch_idx)
        self.log("v_loss", err, prog_bar=True, on_epoch=True)
        return {"v_loss": err}


    def test_step(self, batch, batch_idx):
        err = super().test_step(batch, batch_idx)
        self.log("loss", err, prog_bar=True, on_epoch=True)
        return err
