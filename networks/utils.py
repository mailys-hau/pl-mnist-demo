import torch.nn as nn




# Get correct layer depending on dim
_conv = {1: nn.Conv1d, 2: nn.Conv2d, 3: nn.Conv2d}
_convt = {1: nn.ConvTranspose1d, 2: nn.ConvTranspose2d, 3: nn.ConvTranspose3d}
_max_pooling = {1: nn.MaxPool1d, 2: nn.MaxPool2d, 3: nn.MaxPool3d}



class Reshape(nn.Module):
    def __init__(self, shape):
        super(Reshape, self).__init__()
        self.shape = shape

    def forward(self, x):
        return x.view(*x.shape[:-1], *self.shape)
