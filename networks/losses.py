import inspect as ispc
import torch
import torch.nn as nn
import torch.nn.functional as F


class KLDLoss(nn.Module):
    def __init__(self, recon_loss="MSELoss", w_recon=1, w_kld=1):
        super(KLDLoss, self).__init__()
        self.reconstruction_loss = dict(ispc.getmembers(nn, ispc.isclass))[recon_loss]()
        self.w_recon = w_recon
        self.w_kld = w_kld

    def forward(self, outs, tgs):
        preds, mu, logvar = outs
        recon_loss = self.reconstruction_loss(preds, tgs)
        kld = torch.mean(-0.5 * torch.sum(1 + logvar - mu ** 2 - logvar.exp(), dim=1))
        loss = self.w_recon * recon_loss + self.w_kld * kld
        return {"loss": loss, "recon_loss": recon_loss, "KLD": -kld}
