import torch.nn as nn

from networks.core import EnhancedLightningModule



class _AE(EnhancedLightningModule):
    def __init__(self, encoder, decoder, loss,
                 optimizer={"name": "Adam", "params": {}}, metrics=[]):
        super(_AE, self).__init__(loss, optimizer, metrics)
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

    def training_step(self, batch, batch_idx):
        err = super().training_step(batch, batch_idx)
        self.log("loss", err, prog_bar=True, on_step=True, on_epoch=True)
        return err

    def validation_step(self, batch, batch_idx):
        err = super().validation_step(batch, batch_idx)
        self.log("v_loss", err, prog_bar=True, on_epoch=True)
        return {"v_loss": err}

    def test_step(self, batch, batch_idx):
        err = super().test_step(batch, batch_idx)
        self.log("loss", err, prog_bar=True, on_epoch=True)
        return err


class DenseAE(_AE):
    def __init__(self, cin=1, cout=10, latent=8, loss=nn.CrossEntropyLoss(),
                 optimizer={"name": "Adam", "params": {}}, metrics=[]):
        encoder = nn.Sequential(nn.Flatten(),
                                nn.Linear(28 * 28, 256),
                                nn.ReLU(),
                                nn.Linear(256, 64),
                                nn.ReLU(),
                                nn.Linear(64, latent),
                                nn.ReLU())
        decoder = nn.Sequential(nn.Linear(latent, 32),
                                nn.ReLU(),
                                nn.Linear(32, cout),
                                nn.Softmax())
        super(DenseAE, self).__init__(encoder, decoder, loss, optimizer=optimizer, metrics=metrics)

class ConvAE(_AE):
    def __init__(self, cin=1, cout=1, latent=8, loss=nn.CrossEntropyLoss(),
                 optimizer={"name": "Adam", "params": {}}, metrics=[]):
        encoder = nn.Sequential(nn.Conv2d(cin, 8, 3, stride=2, padding=1), # (8, 14, 14)
                                nn.ReLU(),
                                nn.Conv2d(8, 16, 3, stride=2, padding=1), # (16, 7, 7)
                                nn.ReLU(),
                                nn.Conv2d(16, 32, 3, stride=2), # (32, 3, 3)
                                nn.ReLU(),
                                nn.Flatten(),
                                nn.Linear(32 * 3 * 3, latent),
                                nn.ReLU())
                                #nn.Linear(128, latent))
        decoder = nn.Sequential(#nn.Linear(latent, 128),
                                #nn.ReLU(),
                                nn.Linear(latent, 32 * 3 * 3),
                                nn.ReLU(),
                                nn.Unflatten(1, (32, 3, 3)),
                                nn.ConvTranspose2d(32, 16, 3, stride=2),
                                nn.ReLU(),
                                nn.ConvTranspose2d(16, 8, 3, stride=2, padding=1, output_padding=1),
                                nn.ReLU(),
                                nn.ConvTranspose2d(8, cout, 3, stride=2, padding=1, output_padding=1),
                                nn.Sigmoid())
        super(ConvAE, self).__init__(encoder, decoder, loss, optimizer=optimizer, metrics=metrics)
