import torch
import torch.nn as nn

from networks.core import EnhancedLightningModule
from networks.losses import KLDLoss
from networks.utils import Reshape, _conv, _convt



class _Sampler(nn.Module):
    def __init__(self, hidden, latent):
        super(_Sampler, self).__init__()
        self.mu = nn.Linear(hidden, latent)
        self.logvar = nn.Linear(hidden, latent)

    def forward(self, x):
        mu, logvar = self.mu(x), self.logvar(x)
        # Reparametrization
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu, logvar, mu + eps * std

class _VAE(EnhancedLightningModule):
    def __init__(self, encoder, decoder, sampler, loss,
                 optimizer={"name": "Adam", "params": {}}, metrics=[]):
        super(_VAE, self).__init__(loss, optimizer, metrics)
        self.encoder = encoder
        self.decoder = decoder
        self.sampler = sampler

    def forward(self, x):
        out = self.encoder(x)
        mu, logvar, z = self.sampler(out)
        return [self.decoder(z), mu, logvar]

    def _step(self, batch, batch_idx):
        x, y = batch
        preds = self(x)
        errs = self.loss(preds, y)
        self.preds = preds[0]
        return errs

    def training_step(self, batch, batch_idx):
        errs = super().training_step(batch, batch_idx)
        self.log_dict(errs, prog_bar=True, on_step=True, on_epoch=True)
        return errs["loss"]

    def validation_step(self, batch, batch_idx):
        errs = super().validation_step(batch, batch_idx)
        errs = {f"v_{k}": v for k, v in errs.items()}
        self.log_dict(errs, prog_bar=True, on_epoch=True)
        return errs["v_loss"]

    def test_step(self, batch, batch_idx):
        errs = super().test_step(batch, batch_idx)
        self.log_dict(errs, prog_bar=True, on_epoch=True)
        return errs["loss"]


class ConvVAE(_VAE):
    def __init__(self, cin=1, cout=1, latent=8, loss=KLDLoss(),
                 optimizer={"name": "Adam", "params": {}}, metrics=[]):
        encoder = nn.Sequential(nn.Conv2d(cin, 8, 3, stride=2, padding=1),
                                nn.ReLU(),
                                nn.Conv2d(8, 16, 3, stride=2, padding=1),
                                nn.ReLU(),
                                nn.Conv2d(16, 32, 3, stride=2),
                                nn.ReLU(),
                                nn.Flatten())
        sampler = _Sampler(32 * 3 * 3, latent)
        decoder = nn.Sequential(nn.Linear(latent, 32 * 3 * 3),
                                nn.ReLU(),
                                nn.Unflatten(1, (32, 3, 3)),
                                nn.ConvTranspose2d(32, 16, 3, stride=2),
                                nn.ReLU(),
                                nn.ConvTranspose2d(16, 8, 3, stride=2, padding=1, output_padding=1),
                                nn.ReLU(),
                                nn.ConvTranspose2d(8, cout, 3, stride=2, padding=1, output_padding=1),
                                nn.Sigmoid())
        super(ConvVAE, self).__init__(encoder, decoder, sampler, loss, optimizer, metrics)
